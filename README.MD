
## Сведения о разработчике
Мавлютов Филипп | fmavlyutov@t1-consulting.ru

## Запуск программы
```
> java -jar ./java-course-task-03.jar
```

## Используемые технологии
- JDK Version 1.8

## Требования

#### Hardware:
1. CPU Intel Core i7-7700K
2. RAM 16GB

#### Software:
1. Windows 10 x64
2. JDK Version 1.8

